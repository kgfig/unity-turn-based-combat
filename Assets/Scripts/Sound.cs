﻿using UnityEngine.Audio;
using UnityEngine;

// Mark cusotm class serializable if we want to make it usable in the inspector
[System.Serializable]
public class Sound
{
    public string name;
    public AudioClip clip;
    [Range(0f, 1f)]
    public float volume;

    [Range(0.1f, 3f)]
    public float pitch;

    [HideInInspector]
    public AudioSource source;
}
