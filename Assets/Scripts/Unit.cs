﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    [SerializeField] string unitName;
    [SerializeField] string level;

    [SerializeField] int maxHP;
    [SerializeField] int currentHP;
    [SerializeField] int damage;

    public string getUnitName()
    {
        return unitName;
    }

    public string getLevel()
    {
        return level;
    }

    public int getMaxHP()
    {
        return maxHP;
    }

    public int getCurrentHP()
    {
        return currentHP;
    }

    public int getDamage()
    {
        return damage;
    }

    public void TakeDamage(int dmg)
    {
        currentHP -= dmg;

        // Normalize negative HP if damage exceeds remaining HP
        if (currentHP < 0)
        {
            currentHP = 0;
        }
    }

    public void Heal(int amount)
    {
        currentHP += amount;
        // Normalize HP if it exceeds the max possible value
        if (currentHP > maxHP)
        {
            currentHP = maxHP;
        }
    }

    public bool isDead()
    {
        return currentHP == 0;
    }
}
