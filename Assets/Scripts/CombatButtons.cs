﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombatButtons : MonoBehaviour
{
    [SerializeField] BattleSystem battleSystem;
    [SerializeField] Button buttonAttack, buttonHeal;

    bool isActionEnabled = false;

    // Handle attack button on click event
    // This should only work during the player's turn
    public void OnAttackButton()
    {
        // Do not proceed if action is not enabled 
        // (means that one of the buttons was already clicked once)
        if (!isActionEnabled)
        {
            return;
        }

        // Immediately disable when either button is clicked
        SetActionEnabled(false);
        StartCoroutine(battleSystem.PlayerAttack());
    }

    public void OnHealButton()
    {
        // Do not proceed if action is not enabled 
        // (means that one of the buttons was already clicked once)
        if (!isActionEnabled)
        {
            return;
        }

        // Immediately disable when either button is clicked
        SetActionEnabled(false);
        StartCoroutine(battleSystem.PlayerHeal());
    }

    public void SetActionEnabled(bool actionEnabled)
    {
        isActionEnabled = actionEnabled;

        // Only show buttons when action is enabled
        buttonAttack.gameObject.SetActive(isActionEnabled);
        buttonHeal.gameObject.SetActive(isActionEnabled);
    }

}
