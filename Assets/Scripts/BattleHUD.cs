﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleHUD : MonoBehaviour
{

    [SerializeField] Text nameText;
    [SerializeField] Text levelText;
    [SerializeField] Slider hpSlider;
    [SerializeField] Text hpText;

    Unit unit;

    void Start()
    {

    }

    public void SetHUD(Unit lunit)
    {
        gameObject.SetActive(true);
        unit = lunit;
        nameText.text = unit.getUnitName();
        levelText.text = "Lvl " + unit.getLevel();
        hpSlider.maxValue = unit.getMaxHP();
        UpdateHP();
    }

    public void UpdateHP()
    {
        hpSlider.value = unit.getCurrentHP();
        hpText.text = hpSlider.value + "/" + hpSlider.maxValue;
    }

}
