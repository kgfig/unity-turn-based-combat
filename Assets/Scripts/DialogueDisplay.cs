﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueDisplay : MonoBehaviour
{
    [SerializeField] Text dialogueText;

    public void OnStart(Unit enemyUnit)
    {
        dialogueText.text = "A grumpy " + enemyUnit.getUnitName() + " appears!";
    }

    public void OnPlayerStart()
    {
        dialogueText.text = "Choose an action...";
    }

    public void OnEnemyStart(Unit enemyUnit)
    {
        dialogueText.text = enemyUnit.getUnitName() + " attacks...";
    }

    public void DamageTaken()
    {
        dialogueText.text = "It's effective!";
    }

    public void OnHeal()
    {
        dialogueText.text = "You feel renewed strength!";
    }

    public void BattleWon()
    {
        dialogueText.text = "You won the battle!";
    }

    public void BattleLost()
    {
        dialogueText.text = "Uh-oh :(";
    }
}
