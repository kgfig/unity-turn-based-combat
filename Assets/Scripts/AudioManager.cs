﻿using UnityEngine.Audio;
using System;
using UnityEngine;

/*
 Have a list of sounds where you can add/remove sounds
 */

// current audio clip
// pitch
// volume
// looping

public class AudioConstants
{
    public static string ATTACK = "attack";
    public static string MUSIC = "music";
    public static string ENEMY_ATTACK = "monster";
    public static string WIN = "win";
    public static string HEAL = "heal";
}

public class AudioManager : MonoBehaviour
{

    public Sound[] sounds;
    // Start is called before the first frame update
    void Awake() // Awake is called before Start so it's where we will initialize stuff
    {
        foreach (Sound sound in sounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;

            sound.source.volume = sound.volume;
            sound.source.pitch = sound.pitch;
        }
    }

    void Start()
    {
        // Play music on start
        Play(AudioConstants.MUSIC);
    }

    public void Play(string name)
    {
        Sound sound = Array.Find(sounds, s => s.name == name);
        if (sound != null)
            sound.source.Play();
    }
}
