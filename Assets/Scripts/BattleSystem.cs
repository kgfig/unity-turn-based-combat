﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum BattleState
{
    START,
    PLAYER_START,
    ENEMY_START,
    WON,
    LOST
}
public class BattleSystem : MonoBehaviour
{
    public static float DELAY_START = 2f;
    public static float DELAY_ATTACK = 2f;
    public static float DELAY_NEXT = 1f;
    [SerializeField] private int healAmount;
    // because this is an enum type, possible values
    // appear as a dropdown menu in the Unity editor
    [SerializeField] BattleState state;

    // Add references to our units
    [SerializeField] GameObject playerPrefab;
    [SerializeField] GameObject enemyPrefab;
    CharacterAnimation playerAnimation;
    CharacterAnimation enemyAnimation;
    ParticleSystem playerAttackParticleSystem, playerHealParticleSystem;
    ParticleSystem enemyParticleSystem;

    // Add references to the position of our battle stations where we'll spawn the units
    [SerializeField] Transform playerStationTransform;
    [SerializeField] Transform enemyStationTransform;

    // References to Unit scripts of our player and enemy
    Unit playerUnit;
    Unit enemyUnit;

    // Add reference to the dialogue text
    [SerializeField] BattleHUD playerHUD;
    [SerializeField] BattleHUD enemyHUD;
    [SerializeField] DialogueDisplay dialogueDisplay;
    [SerializeField] CombatButtons combatButtons;

    AudioManager audioManager;

    void Awake()
    {
        audioManager = FindObjectOfType<AudioManager>();
    }

    void Start()
    {
        // Set up battle on start
        StartCoroutine(SetupBattle());
    }

    // Everything related to setup
    // Any function that will wait is a coroutine that has to have the IEnumerator type
    // TODO compare this StartCoroutine with regular delay
    IEnumerator SetupBattle()
    {
        // Set initial state
        state = BattleState.START;


        // Delay, Show player first, introduce delay, show enemy, introduce delay, then finally start player turn
        yield return StartCoroutine(SpawnPlayer(playerPrefab, playerStationTransform.position));
        yield return StartCoroutine(SpawnEnemy(enemyPrefab, enemyStationTransform.position));
        Debug.Log(playerAnimation);
        Debug.Log(enemyAnimation);

        // yield return new WaitForSeconds(DELAY_START);

        combatButtons.SetActionEnabled(true);
        PlayerTurn();
    }

    IEnumerator SpawnPlayer(GameObject prefab, Vector3 position)
    {
        // Spawn player and enemy units on the battle stations
        // Instead of using the Transform directly to instantiate the prefab, we use the alternative method signature
        // so we're just passing the position and rotation since the scale of the battle station does not modify
        // the scale of our player and enemy unit
        // Pass constant for no rotation
        yield return new WaitForSeconds(DELAY_NEXT);

        GameObject gameObject = Instantiate(prefab, position, Quaternion.identity);
        playerUnit = gameObject.GetComponent<Unit>();
        playerAnimation = GetChildCharacterAnimation(gameObject);
        playerAttackParticleSystem = GetChildParticleSystem(gameObject, 2);
        playerHealParticleSystem = GetChildParticleSystem(gameObject, 1);

        playerHUD.SetHUD(playerUnit);
        yield return new WaitForSeconds(DELAY_NEXT);
    }

    IEnumerator SpawnEnemy(GameObject prefab, Vector3 position)
    {
        GameObject gameObject = Instantiate(prefab, position, Quaternion.identity);
        enemyUnit = gameObject.GetComponent<Unit>();
        enemyAnimation = GetChildCharacterAnimation(gameObject);
        enemyParticleSystem = GetChildParticleSystem(gameObject, 1);

        dialogueDisplay.OnStart(enemyUnit);
        enemyHUD.SetHUD(enemyUnit);
        audioManager.Play(AudioConstants.ENEMY_ATTACK);

        yield return new WaitForSeconds(DELAY_START);
    }

    private CharacterAnimation GetChildCharacterAnimation(GameObject gameObject)
    {
        return gameObject.transform.GetChild(0).gameObject.GetComponent<CharacterAnimation>();
    }

    private ParticleSystem GetChildParticleSystem(GameObject gameObject, int index)
    {
        return gameObject.transform.GetChild(index).gameObject.GetComponent<ParticleSystem>();
    }

    void PlayerTurn()
    {
        state = BattleState.PLAYER_START;
        dialogueDisplay.OnPlayerStart();
        // Enable action only during the player's turn
        combatButtons.SetActionEnabled(true);
    }

    public BattleState GetState()
    {
        return state;
    }

    public IEnumerator PlayerAttack()
    {
        if (state != BattleState.PLAYER_START)
        {
            // cannot return from a coroutine
            // use yield break to break iteration
            yield break;
        }

        audioManager.Play(AudioConstants.ATTACK);
        playerAnimation.SetAttacking(true);
        enemyAnimation.SetHurt(true);
        playerAttackParticleSystem.Play();

        yield return new WaitForSeconds(DELAY_ATTACK);
        playerAnimation.SetAttacking(false);
        enemyAnimation.SetHurt(false);

        // damage the enemy
        enemyUnit.TakeDamage(playerUnit.getDamage());
        enemyHUD.UpdateHP();
        dialogueDisplay.DamageTaken();

        // yield return new WaitForSeconds(DELAY_ATTACK);

        yield return new WaitForSeconds(DELAY_NEXT);

        // check if dead
        if (enemyUnit.isDead())
        {
            enemyAnimation.SetDead(true);
            StartCoroutine(Won());
        }
        else
        {
            StartCoroutine(EnemyTurn());
        }
    }

    IEnumerator EnemyTurn()
    {
        // Update state
        state = BattleState.ENEMY_START;
        // Update dialogue
        dialogueDisplay.OnEnemyStart(enemyUnit);
        enemyAnimation.SetAttacking(true);
        enemyParticleSystem.Play();
        audioManager.Play(AudioConstants.ENEMY_ATTACK);

        yield return new WaitForSeconds(DELAY_ATTACK);

        playerAnimation.SetHurt(true);
        yield return new WaitForSeconds(DELAY_ATTACK);
        playerAnimation.SetHurt(false);

        // After some delay, deal some damage and update HUD        
        playerUnit.TakeDamage(enemyUnit.getDamage());
        playerHUD.UpdateHP();
        enemyAnimation.SetAttacking(false);

        yield return new WaitForSeconds(DELAY_ATTACK);

        // After some delay, update the state again
        if (playerUnit.isDead())
        {
            playerAnimation.SetDead(true);
            Lost();
        }
        else
        {
            PlayerTurn();
        }
    }


    public IEnumerator PlayerHeal()
    {
        if (state != BattleState.PLAYER_START)
        {
            yield break;
        }

        playerHealParticleSystem.Play();
        audioManager.Play(AudioConstants.HEAL);
        yield return new WaitForSeconds(DELAY_ATTACK);

        playerUnit.Heal(healAmount);
        playerHUD.UpdateHP();
        dialogueDisplay.OnHeal();

        yield return new WaitForSeconds(DELAY_NEXT);

        StartCoroutine(EnemyTurn());
    }

    IEnumerator Won()
    {
        state = BattleState.WON;
        dialogueDisplay.BattleWon();
        playerAnimation.SetWon(true);
        audioManager.Play(AudioConstants.WIN);
        yield return new WaitForSeconds(DELAY_START);
    }

    public void Lost()
    {
        state = BattleState.LOST;
        dialogueDisplay.BattleLost();
    }

}
