﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimation : MonoBehaviour
{

    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void SetAttacking(bool isAttacking)
    {
        animator.SetBool("IsAttacking", isAttacking);
    }

    public void SetDead(bool isDead)
    {
        animator.SetBool("IsDead", isDead);
    }

    // public void SetHealing(bool isHealing)
    // {
    //     animator.SetBool("IsHealing", isHealing);
    // }

    public void SetHurt(bool isHurt)
    {
        animator.SetBool("IsHurt", isHurt);
    }

    public void SetWon(bool won)
    {
        animator.SetBool("Won", won);
    }
}
